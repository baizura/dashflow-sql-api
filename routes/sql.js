var connection = require('../database/sql');
var Request = require('tedious').Request
var express = require('express');
var router = express.Router();
var TYPES = require('tedious').TYPES;  
const request=require('request')
const csvtojson =require('csvtojson')
/* GET users listing. */
router.post('/', function(req, res, next) {
    console.log(req.body)
    var options = { keepNulls: true };
    // instantiate - provide the table where you'll be inserting to, options and a callback
    var bulkLoad = connection.newBulkLoad('dbo.Team', options, function (error, rowCount) {
        console.log('inserted %d rows', rowCount);
    });
    // setup your columns - always indicate whether the column is nullable
    bulkLoad.addColumn('id', TYPES.NVarChar, { length: 50, nullable: false });
    bulkLoad.addColumn('Name', TYPES.NVarChar, { length: 50, nullable: true });
    bulkLoad.addColumn('Email', TYPES.NVarChar, { length: 50, nullable: true });
    bulkLoad.addColumn('Role', TYPES.NVarChar, { length: 50, nullable: true });
    csvtojson()
    .fromStream(request.get(req.body.url))
    .subscribe((json)=>{
       console.log(json)
    }).then((jsondata) => {
        console.log(jsondata)
        for (var i = 0; i < jsondata.length; i++) { 
            var id = jsondata[i]["id"],
             Name = jsondata[i]["Name"], 
             Email = jsondata[i]["Email"],
             Role = jsondata[i]["Role"];
            // add rows
            console.log( id + Name + Email + Role)
            bulkLoad.addRow({ id: id, Name: Name, Email: Email,Role: Role });
          } 
          connection.execBulkLoad(bulkLoad);
    });
   
});

module.exports = router;
